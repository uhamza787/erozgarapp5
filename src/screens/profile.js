import { StyleSheet, TouchableOpacity, View, Image } from "react-native";
import { Camera, CameraType } from "expo-camera";
import React, { useRef, useState } from "react";
import { Ionicons } from "@expo/vector-icons";

export default function Profile({ route }) {
  const [picURI, setPicURI] = useState();
  const [cameraType, setCameraType] = useState(CameraType.front);

  const [permission, requestPermission] = Camera.useCameraPermissions();
  requestPermission();

  const cameraRef = useRef();

  const onCameraPress = () => {
    cameraRef.current
      .takePictureAsync()
      .then((response) => {
        setPicURI(response.uri);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onCameraFlipPress = () => {
    if (cameraType === CameraType.front) {
      setCameraType(CameraType.back);
    } else {
      setCameraType(CameraType.front);
    }
  };

  return (
    <View>
      <View style={{ width: "100%", height: "50%", backgroundColor: "red" }}>
        <Camera
          style={{ width: "100%", height: "100%" }}
          type={cameraType}
          ref={cameraRef}
        >
          <View style={styles.cameraCon}>
            <TouchableOpacity onPress={onCameraPress}>
              <Ionicons name={"camera"} size={100} color={"white"} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={onCameraFlipPress}>
            <Ionicons name={"eye"} size={100} color={"white"} />
          </TouchableOpacity>
        </Camera>
      </View>
      <Image style={{ width: 300, height: 300 }} source={{ uri: picURI }} />
    </View>
  );
}

const styles = StyleSheet.create({
  cameraCon: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
});
