import { View, TextInput, StyleSheet } from "react-native";
import React from "react";
import Ionicons from "@expo/vector-icons/Ionicons";
const CustomInput = ({ icon, iconColor, placeholderText, onChangeText }) => {
  return (
    <View style={styles.inputCon}>
      <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        placeholder={placeholderText}
      />
      <Ionicons name={icon} color={iconColor} size={22} />
    </View>
  );
};

export { CustomInput };

const styles = StyleSheet.create({
  input: {
    width: "95%",
  },
  inputCon: {
    padding: 10,
    alignItems: "center",
    flexDirection: "row",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "black",
    margin: 10,
  },
});
